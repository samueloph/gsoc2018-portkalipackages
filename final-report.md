![](images/GSoC-logo-horizontal-800.png)

# ![](images/openlogo-100.png) Port Kali Packages to Debian
https://summerofcode.withgoogle.com/projects/#5969146995539968  
https://wiki.debian.org/SummerOfCode2018/Projects/PortKaliPackages

# Mentors
Raphaël Hertzog \<hertzog@debian.org\>  
Gianfranco Costamagna \<locutusofborg@debian.org\>

## whoami
Samuel Henrique \<samueloph@debian.org\> is a Debian Developer (became one during GSOC) who works mainly on the Debian Security Tools Packaging Team since ~2016.  
I'm highly interested in Devops, Cloud, Security and Debian, you can find me on the internet by my handle *@samueloph*.

# Introduction
My project consisted of working on the Security Tools Packaging Team with the task of importing Kali Packages to Debian.

Kali is by far the most used distribution for pentensting and forensics, and its policy being less strict than Debian's allows it to be able to package more tools with less people. The work consisted mainly of analyzing these packages and doing the work needed in order for them to be accepted on Debian.

The main source of information was the Kali packages tracker which lists the packages that are not on Debian yet: http://pkg.kali.org/derivative/kali-dev/

## Presentation of GSOC during DebConf18
I had about 8 minutes for a presentation at DebConf18 where I briefly talk about the project, the whole presentation was shared between participants of GSOC and Outreachy, mine starts at about 8:25.

https://gemmei.ftp.acc.umu.se/pub/debian-meetings/2018/DebConf18/2018-07-31/gsoc-session.webm 

# Results

## Upstream contributions/discussions
### cowpatty
https://github.com/joswr1ght/cowpatty/issues/1  
https://github.com/joswr1ght/cowpatty/pull/2  
https://github.com/joswr1ght/cowpatty/pull/3  
https://github.com/joswr1ght/cowpatty/pull/4

### changeme
https://github.com/ztgrace/changeme/pull/69

### weevely3
https://github.com/epinna/weevely3/issues/84  
https://github.com/epinna/weevely3/issues/88  
https://github.com/epinna/weevely3/pull/87  
https://github.com/epinna/weevely3/pull/89  
https://github.com/epinna/weevely3/pull/90

### smbmap
https://github.com/ShawnDEvans/smbmap/pull/23

### mfoc
https://github.com/nfc-tools/mfoc/pull/56

### wig
https://github.com/jekyc/wig/issues/36  
https://github.com/jekyc/wig/pull/35

### zaproxy
https://groups.google.com/forum/?utm_medium=email&utm_source=footer#!msg/zaproxy-develop/wN6HtAKOFfw/gcYTiGR6BQAJ

## Script to check Kali packages' status
I had to make a script to help me choose and analyze which packages I should work on and how much work do they needed.  
This ended up being one of the most important contributions from my project because it's something that i'll continue using on the future as I work on adding new Kali packages on Debian and it'll also benefit others doing the same.

The scripts accepts a file containing names of packages to test, then it downloads all of their packaging repositories from Kali, tries to build each one of the packages and outputs the result in a csv file, which is used to generate the spreadsheet mentioned on the topic below.

https://salsa.debian.org/samueloph/gsoc-kali/blob/master/kali-packages-checker

## Report of Kali packages and its state (for Debian packaging)
The spreadsheet made of the script's csv output, I added comments on the columns with explanations, colors, and two more columns for manual notes on each package, which I use to add things that are not detected by the script and extra information.

https://salsa.debian.org/samueloph/pkg-security-team-kali-spreadsheet/tree/master  
https://docs.google.com/spreadsheets/d/1muSrob3G1c7ZwHlxfDAho4qtdJ9pZNycCKT9kWyIJkw

## Packages added to Debian
The ITP bugs opened for the packages that I worked on, all of them are on salsa.debian.org, either on under the [Security Tools Packaging Team organization](https://salsa.debian.org/pkg-security-team) at salsa or the fitted organization for the package in case of a lib, eg.: on the [Python DPMT organization](https://salsa.debian.org/python-team/modules/) at salsa.

Also note that as of now, some of the packages are on the [NEW queue](https://ftp-master.debian.org/new.html) and "responder" has some problems that I still have to fix before sending it to the NEW queue again.

https://bugs.debian.org/cgi-bin/pkgreport.cgi?usertag=samueloph@debian.org

### Packages already accepted (as of 2018-08-16)
* changeme
* mfoc
* mfuck
* python-crcelk
* python-libnmap
* python-smoke-zephyr
* weevely
* wig

### Packages in the NEW queue (as of 2018-08-16)
* cowpatty
* o-saft
* python-shodan
* smbmap
* termineter

### Packages which needs fixes to re-enter NEW
* responder

## Future work
There are two tools that I really want to see it on Debian but couldn't do it on this project: metasploit and zaproxy.

### Metasploit
I prepared an analysis of its ruby dependencies, as the main problem right now is that Kali is running "bundle install" during build and we can't do that on Debian.

The analysis is on the second tab of the spreadsheet (called "metasploit") and there we can see that there's at least 40 dependencies missing on Debian.

Another important thing that we have to do is to write good autopkgtests for metasploit, so we can detect when it breaks due to dependencies updating on Debian either report to upstream or send patches fixing it :)

The good thing is that while working on the project, i got in contact with at least 2 other people really interested in helping on that, so i truly believe we will be able to accomplish this.

There was also a relevant discussion on the debian-devel mailing list started by one of my mentors, Raphaël Hertzog \<hertzog@debian.org\>, the thread is called  [What can Debian do to provide complex applications to its users?](https://lists.debian.org/debian-devel/2018/02/msg00295.html) and it has an interesting discussion, it's something that i discussed with a few people during DebConf and that could help Debian deliver tools that are not yet on the "Debian level of packaging" to its users.

### Zaproxy
I started brief discussion on their [developer mailing list](https://groups.google.com/forum/?utm_medium=email&utm_source=footer#!msg/zaproxy-develop/wN6HtAKOFfw/gcYTiGR6BQAJ) regarding its bundled libraries.

There is a third tab on the spreadsheet called "zaproxy" with the state of their bundled Java libraries.  
Upstream was very receptive and they are willing to help us on this, so i'll continue working on that in order to provide Debian users with this great tool. I think it will take some time until we are able to ship this package though.

### All the other packages
Kali currently has at least 471 packages that Debian doesn't, so there's lots of work ahead, i'm planning to start mentoring people who wants to start packaging on the team in order to get more people helping while at the same time be able to help others and learn during the process.

# Final thoughts
I'm very grateful for having the high skilled mentors I had (Raphaël and Gianfranco) during my project, they were very helpful and gave me awesome feedbacks.  
The amount of experience I gathered during this project is huge and i will not forget it.  
I did want to have packaged more software, but I think analyzing all of the ones i didn't end up working on was worth it, as I'll continue packaging more stuff after GSOC and i would have to do that anyway.

Thank you Debian and Google for the amazing experience!
